﻿using UnityEngine;
using System.Collections;

public class CameraController : MonoBehaviour {

  public GameObject mPlayer;
  private Vector3 offset;
  
	// Use this for initialization
	void Start () {
      offset = transform.position - mPlayer.transform.position;  
	}
	
	// Update is called once per frame
	void LateUpdate () {
      transform.position = mPlayer.transform.position + offset;
	}
}
