﻿using UnityEngine;
using System.Collections;

public class PauseButtonController : MonoBehaviour {
    private string stateMachineTag = "stateGameController";
    
    public void onClick() {
        Debug.Log("Click on PauseButton");
        GameObject.Find(stateMachineTag)
            .GetComponent<StateMachine>().setPause();
    }
}
