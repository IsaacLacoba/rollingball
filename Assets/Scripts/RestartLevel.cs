﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class RestartLevel : MonoBehaviour {
    
    public void onClick() {
        SceneManager.LoadScene(SceneManager
                               .GetActiveScene().buildIndex);
    }
}
