﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class BallController : MonoBehaviour {
    public bool mStart;
    
    private Rigidbody mRigidbody;
    public float mSpeed = 10.0f;    
    private Vector3 mCorrectionFactor;
    private Vector3 mVectorSpeed;
    Vector3 mAcceleration;
    
    public Text scoreText;
    private string pickupTag = "Pickup";

    public int mMaxScore = 8;
    public int mScore;
    
    // Use this for initialization
    void Start() {
        mRigidbody = GetComponent<Rigidbody>();
        mAcceleration = new Vector3(0, 0, 0);
        mVectorSpeed = new Vector3(mSpeed, mSpeed/2, mSpeed);
        mCorrectionFactor = new Vector3(0, 0.4f, 0);

        mScore = 0;

    }

    
    public void FixedUpdate() {
        if (!mStart) {
            return;
        }

        setScoreText();
        updateAcceleration();
        mRigidbody.AddForce(Vector3.Scale(mAcceleration, mVectorSpeed) -
                            mCorrectionFactor);
    }

    public void OnTriggerEnter(Collider collideObject) {
        if (collideObject.gameObject.CompareTag(pickupTag)) {
           collideObject.gameObject.SetActive(false);
           mScore++;
           setScoreText();
        }        
    }

    
    private void updateAcceleration() {
        Vector3 inputAcceleration = Input.acceleration + mCorrectionFactor;
        mAcceleration.x = inputAcceleration.x;
        mAcceleration.z = inputAcceleration.y;
        
        if (hasDetectShake()) {
            Debug.Log("shake decteted!!");
            mAcceleration.y = Mathf.Abs(inputAcceleration.z);
        }
    }

    private bool hasDetectShake() {
        return false;
    }

    private void setScoreText() {
        scoreText.text = "Score: " + (mScore);
    }
}
