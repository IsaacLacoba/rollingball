﻿using UnityEngine;
using System.Collections;

public class Rotater : MonoBehaviour {
    public Vector3 rotation = new Vector3(1, 1, 1);
    public ParticleSystem explotion;
    
    public void Start() {
    
    }
    
    public void Awake() {
        explotion = GetComponent<ParticleSystem>();
    }
    
    // Update is called once per frame
    void Update () {
        transform.Rotate(rotation * Time.deltaTime);
    }
    
    void OnTriggerEnter(Collider colliderObject) {
        Debug.Log("onCollisionEnter");
        explotion.Play();
    }    
}
