﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
    
public class StateMachine : MonoBehaviour {
    public bool mStart;
    
    //TODO: refactor. User enumeration
    private bool mPaused, win;
    public Text mStateTitleText;
    
    private string pauseTitle = "Pause";
    private string winTitle = "You Win!";

    public GameObject mPlayer, mMenuPanel, mPauseButton, MainText;

    private float maxScore;

    
    // Use this for initialization
    public void Start() {
        maxScore = mPlayer.GetComponent<BallController>().mMaxScore;
        mStateTitleText.text = "";
        
    }
	
    // Update is called once per frame
    public void Update() {
        checkForTouches();
        if (!mStart) {
            return;
        }
        
        win = mPlayer.GetComponent<BallController>()
            .mScore >= maxScore;      
        //TODO: Create a binder and refactor that to a if state == then map[state]() => do action
        if (win) {
            setWinState();
        } else if (mPaused) {
            pauseGame();
        } else {
            resumeGame();
        }                            
    }

    private bool checkForTouches() {
        if (Input.touchCount == 0) {
            Debug.Log ("touches: " + Input.touchCount);
                    
            return false;
        }
        Debug.Log ("touches: " + Input.touchCount);
        startGame();
        return true;
    }

    private void startGame() {               
        MainText.SetActive(false);
        mPauseButton.SetActive(true);
        mStart = true;
        mPlayer.GetComponent<BallController>().mStart = true;
    }
    
    private void pauseGame() {
        freezeGame(true);
        mStateTitleText.text = pauseTitle;
    }

    private void resumeGame() {
        freezeGame(false);
        mStateTitleText.text = "";
    }
    private void setWinState() {
        freezeGame(true);
        mStateTitleText.text = winTitle;
        mPauseButton.SetActive(false);
        mMenuPanel.SetActive(true);
        Debug.Log("setWinState function");
    }

    private void freezeGame(bool freeze) {
        Time.timeScale = freeze? 0: 1;
    }

    public void setPause() {
        mPaused = !mPaused;
    }
}
